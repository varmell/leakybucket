all: buffers

runOnly:
	./buffers

run: buffers clear
	./buffers

buffers: main.o cell.o bufferCell.o token.o client.o save.o test.o stats.o
	g++ -std=c++11 -o buffers main.o cell.o bufferCell.o token.o client.o test.o stats.o save.o -W -Wall

save.o: sources/save.cpp headers/save.h
	g++ -std=c++11 -c sources/save.cpp -W -Wall

client.o: sources/client.cpp headers/client.h
	g++ -std=c++11 -c sources/client.cpp -W -Wall

cell.o: sources/cell.cpp headers/cell.h
	g++ -std=c++11 -c sources/cell.cpp -W -Wall

token.o: sources/token.cpp headers/token.h
		g++ -c sources/token.cpp -W -Wall

bufferCell.o: sources/bufferCell.cpp headers/bufferCell.h
	g++ -std=c++11 -c sources/bufferCell.cpp -W -Wall

test.o: sources/test.cpp headers/test.h
	g++ -std=c++11 -c sources/test.cpp -W -Wall

stats.o: sources/stats.cpp headers/stats.h
	g++ -std=c++11 -c sources/stats.cpp -W -Wall

main.o: sources/main.cpp headers/cell.h headers/bufferCell.h headers/token.h headers/client.h headers/save.h headers/stats.h
	g++ -std=c++11 -c sources/main.cpp -W -Wall

clear:
	rm *.o
