#include "token.h"
#include "bufferCell.h"
#ifndef CLIENT_H
#define CLIENT_H


class Client
{
	private:
    int idClient;			// identificateur propre à chaque client
		bool on; 			//Booléen montrant l’activité
		float geneTokensRate;		// fréquence de génération des jetons
		float actualTransmitRate;	// taux d’émission actuel du client
    int nbTransmitCell;		// nombre de cellules émises
		int nbLostCell;			// nombre de cellules perdues
    BufferCell cells;	// buffer contenant les cellules en attente du client
    Token tokens;	// buffer contenant les jetons générés

	public :
		Client ( );			// Constructeur simple
		Client (int id, float transmitRate, float geneTokensRate);
        // Constructeur avec entrée de paramètres pour les attributs
        //idClient l’identifiant du client, te le taux d’emission actuel des cellules et tj le taux de génération des jetons
    ~Client( );			// Destructeur

		bool get_on();
		void set_on(bool aBool);
		int get_idClient ( );
		void set_idClient (int idClient );
		float get_tokens_rate ( );
		void set_tokens_rate (float tokensRate);
    float get_transmit_rate ( );
		void set_transmit_rate (float transmitRate);
		int get_nb_cell_transmit ( );
		void set_nb_cell_transmit (int nbCellTransmit);
		int get_nb_lost_cell ( );
		void set_nb_lost_cell (int nbCellLost);
		BufferCell get_cells();
		Token get_tokens();
};
#endif // CLIENT_H
