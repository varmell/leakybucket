#include "cell.h"
//#include <QGraphicsEllipseItem>
#include <string>
#ifndef BUFFERCELL_H_INCLUDED
#define BUFFERCELL_H_INCLUDED

using std::string;

class BufferCell
{
  private:
    Cell* cells;
    int length;
    //QGraphicsRectItem cellGraph[5];
  public:
    BufferCell();
    BufferCell(int aLength);
    ~BufferCell();
    bool add_Cell(Cell aCell);
    void take_Cell();
    void move_to_left();
    string to_String();
    bool cells_void();
};

#endif
