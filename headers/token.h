//#include <QGraphicsEllipseItem>
#include <iostream>
#ifndef TOKEN_H_INCLUDED
#define TOKEN_H_INCLUDED

using std::string;

/*
  Cette classe permet de représenter
  l'ensemble de la file d'attente des jetons
  d'un client
*/
class Token
{
  private:
    int length;
    bool* bufferToken;
    //QGraphicsEllipseItem tokenGraph[5];
  public:
    Token();
    Token(int length);
    ~Token();

    int get_length();
    //QGraphicsEllipseItem* get_tokenGraph();
    bool* get_bufferToken();

    void add_Token();
    void move_to_left();
    string to_String();
};

#endif
