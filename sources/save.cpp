#include "../headers/save.h"
#include "../headers/stats.h"
using namespace std;

/** Entrées : Client c
* sortie aucune
* crée un fichier de sauvegarde en fonction de la date et heure
* Et écrit les informations lié aux clients durant la simulation
**/

void ecriture(Client* c, int length)
{
  time_t temps;
  struct tm datetime;
  char  format[32];

  time(&temps);
  datetime = *localtime(&temps);

  strftime(format, 32, "%Hh%M %d-%m-%Y.txt", &datetime);
  int i = 0;
  ofstream fichier(format, ios::out | ios::trunc);
  if (fichier)
  {
    fichier << "Statistiques globales" << endl;
    fichier << "Nombre-cellules-emises: " << total_sent_cells(c, length) <<endl;
    fichier << "Nombre-cellules-perdues: " << total_lost_cells(c, length) <<endl;
    fichier << "taux-perte-global: " << lost_cell_average(c, length) <<" %"<<endl;
    fichier << endl;
    fichier << "Statistiques par Client" <<endl;
      for (i = 0; i < length; i ++)
      {
      //Boucle selon le nombre de clients de la simulation
      fichier << "Clients-numero: " << c[i].get_idClient()  << endl;
      fichier << "Taux-emission-actuel: " << c[i].get_transmit_rate()<<" /s"<< endl;
      fichier << "Taux-generation-jetons: " << c[i].get_tokens_rate() <<" /s"<<endl;
      fichier << "Nombre-cellule-emises: " << c[i].get_nb_cell_transmit() <<endl;
      fichier << "Nombres-cellule-perdues: " << c[i].get_nb_lost_cell()<<endl;
      fichier << "Taux-pertes-cellules: " << lost_cells_rate(c[i]) <<" %"<<endl<<endl;
    }
    fichier.close();
  }
}

/** Entrées : String path
* sortie aucune
* Ecrit dans les informations recupere dans un fichier
* les données des clients.
**/

void lecture (string path, Client* c)
{
  ifstream fichier(path.c_str());
  if (fichier)
  {
      float lost_cell_average, lost_cells_rate;
      int total_sent_cells, total_lost_cells, id_client, transmist_rate, tokens_rate, nb_cell_transmit, nb_lost_cell;
      string tempo;
      int nb_client_actif;
      int i = 0;

      getline(fichier, tempo);
      fichier >> tempo >> total_sent_cells;
      cout << "Nombres cellules emises: " << total_sent_cells <<endl;
      fichier >> tempo >> total_lost_cells;
      cout << "Nombres cellules perdues: " <<total_lost_cells << endl;
      fichier >> tempo >> lost_cell_average >> tempo;
      cout <<"Taux perte global: " <<lost_cell_average<<endl<<endl;
      getline(fichier, tempo);
      getline(fichier, tempo);

      while (getline(fichier, tempo))
      {
        fichier >> tempo >> id_client;
        c[i].set_idClient(id_client);
        cout << "client numero " << id_client << endl;
        fichier >> tempo >> transmist_rate >> tempo;
        c[i].set_transmit_rate(transmist_rate);
        cout << "taux emission: "<< transmist_rate <<endl;
        fichier >> tempo >> tokens_rate >> tempo;
        c[i].set_tokens_rate(tokens_rate);
        cout << "Taux emmission jeton: " << tokens_rate << endl;
        fichier >> tempo >> nb_cell_transmit;
        c[i].set_nb_cell_transmit(nb_cell_transmit);
        cout << "Nombre de cellules emise: "<<nb_cell_transmit<<endl;
        fichier >> tempo >> nb_lost_cell;
        c[i].set_nb_lost_cell(nb_lost_cell);
        cout << "Nombre de cellules perdu: "<<nb_lost_cell<<endl;
        fichier >> tempo >> lost_cells_rate >> tempo;
        cout << "Taux perte cellulles " << lost_cells_rate<<endl<<endl;
        getline(fichier, tempo);
        i ++;
      }

      fichier.close();
  }
}
