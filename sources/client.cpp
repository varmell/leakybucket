#include "../headers/client.h"

/**
    * Description : Constructeur basique de la classe Client
    * paramètre : /
    * sortie : /
**/
Client::Client()
{
    //ctor
}

/**
    * Description : Constructeur de la classe Client
    * paramètre :
        int id : l'identifiant du client
        float transmitRate : le taux d'emission actuel du client
        float geneTokensRate : le taux d'emission des jetons du client
    * sortie : /
**/
Client::Client (int id, float transmitRate, float geneTokensRate)
{
    this->idClient = id;
    this->on = false; //Client inactif a l'initialisation
    this->geneTokensRate = geneTokensRate;
    this->actualTransmitRate = transmitRate;
    this->nbTransmitCell = 0; //0 cellules émises a l'initialisation
    this->nbLostCell = 0; //0 cellules perdues a l'initialisation
    this->cells =  BufferCell(10);
    this->tokens = Token(10);
}


/**
    * Description : Destructeur de la classe Client
    * paramètre : /
    * sortie : /
**/
Client::~Client()
{
    //Destructeur
}

/**
    * Description : méthode permettant de lire et utiliser la valeur de l'attribut on
    * paramètre : /
    * sortie : on
**/
bool Client::get_on()
{
    return this->on;
}

/**
    * Description : méthode permettant de lire et utiliser la valeur de l'attribut idClient
    * paramètre : /
    * sortie : idClient
**/
int Client::get_idClient()
{
    return this->idClient;
}

/**
    * Description : méthode permettant de lire et utiliser la valeur de l'attribut nbTransmitCell
    * paramètre : /
    * sortie : nbTransmitCell
**/
int Client::get_nb_cell_transmit()
{
    return this->nbTransmitCell;
}

/**
    * Description : méthode permettant de lire et utiliser la valeur de l'attribut nbLostCell
    * paramètre : /
    * sortie : nbLostCell
**/
int Client::get_nb_lost_cell()
{
    return this->nbLostCell;
}

/**
    * Description : méthode permettant de lire et utiliser la valeur de l'attribut actualTransmitRate
    * paramètre : /
    * sortie : actualTransmitRate
**/
float Client::get_transmit_rate()
{
    return this->actualTransmitRate;
}

/**
    * Description : méthode permettant de lire et utiliser la valeur de l'attribut geneTokensRate
    * paramètre : /
    * sortie : geneTokensRate
**/
float Client::get_tokens_rate()
{
    return this->geneTokensRate;
}


/**
    * Description : méthode permettant de remplacer la valeur de l'attribut on par celle entrée en paramètre
    * paramètre : int aBool
    * sortie : /
**/
void Client::set_on(bool aBool)
{
    this->on = aBool;
}


/**
    * Description : méthode permettant de remplacer la valeur de l'attribut idClient par celle entrée en paramètre
    * paramètre : int id
    * sortie : /
**/
void Client::set_idClient(int idClient)
{
    this->idClient = idClient;
}


/**
    * Description : méthode permettant de remplacer la valeur de l'attribut nbCellTransmit par celle entrée en paramètre
    * paramètre : int nbCellTransmit
    * sortie : /
**/
void Client::set_nb_cell_transmit(int nbCellTransmit)
{
    this->nbTransmitCell = nbCellTransmit;
}


/**
    * Description : méthode permettant de remplacer la valeur de l'attribut nbLostCell par celle entrée en paramètre
    * paramètre : int nbLostCell
    * sortie : /
**/
void Client::set_nb_lost_cell(int nbLostCell)
{
    this->nbLostCell = nbLostCell;
}


/**
    * Description : méthode permettant de remplacer la valeur de l'attribut transmitRate par celle entrée en paramètre
    * paramètre : float transmitRate
    * sortie : /
**/
void Client::set_transmit_rate(float transmitRate)
{
    this->actualTransmitRate = transmitRate;
}


/**
    * Description : méthode permettant de remplacer la valeur de l'attribut tokensRate par celle entrée en paramètre
    * paramètre : float tokensRate
    * sortie : /
**/
void Client::set_tokens_rate(float tokensRate)
{
    this->geneTokensRate = tokensRate;
}

/**
* Accesseur de l'attribut cells
* Entrées : aucun
* Sortie : le tableau des cellules
*/
BufferCell Client::get_cells()
{
  return this->cells;
}

/**
* Accesseur de l'attribut tokens
* Entrées : aucun
* Sortie : le tableau des jetons
*/
Token Client::get_tokens()
{
  return this->tokens;
}
