#include "../headers/test.h"
#include <iostream>

using namespace std;

/** Entrées : aucune
* sortie aucune
* crée un objet Token grace au constructeur vide
* auteur : Dylan
**/
void test_void_constructor()
{
  Token test_token;
  cout << "Constructeur vide fait !" << endl;
}

/** Entrées : un entier
* sortie aucune
* crée un objet Token grace au constructeur prenant
* en paramètre la taille du tableau de jetons
* auteur : Dylan
**/
void test_constructor(int length)
{
  Token test_token(length);
  cout << "Constructeur avec length fait !" << endl;
}

/** Entrées : aucune
* sortie aucune
* crée un objet Token ayant pour taille 5
* affiche le retour la méthode get_length qui doit
* être 5
* auteur : Dylan
**/
void test_get_length()
{
  Token test_token(5);
  cout << "token créer avec une taille de 5" << endl;
  cout << "length de test_token est " << test_token.get_length() << endl;
}

/** Entrées : aucune
* sortie aucune
* crée un objet Token ayant pour taille 5
* exécute la méthode get_bufferToken sur chaque case
* du tableau de jetons. On doit avoir 5 "1" d'afficher
* auteur : Dylan
**/
void test_get_bufferToken()
{
  Token test_token(5);
  cout << "on doit avoir 5 jetons" << endl;
  for(int i = 0 ; i < test_token.get_length();i ++)
  {
    cout << test_token.get_bufferToken()[i];
  }
  cout << endl;
}

/** Entrées : aucune
* sortie aucune
* crée un objet Token ayant pour taille 5
* exécute la méthode move_to_left et affiche
* les cases du tableau de jetons
* on doit avoir 4 "1" et 1 "0"
* auteur : Dylan
**/
void test_move_to_left()
{
  Token test_token(5);
  test_token.move_to_left();
  cout << "on doit avoir 4 jetons" << endl;
  for(int i = 0 ; i < test_token.get_length();i ++)
  {
    cout << test_token.get_bufferToken()[i];
  }
  cout << endl;
}

/** Entrées : aucune
* sortie aucune
* crée un objet Token ayant pour taille 5
* exécute la commande move_to_left
* puis add_Token
* et enfin affiche les cases du tableau de jetons
* on doit avoir 5 "1"
* auteur : Dylan
**/
void test_add_token()
{
  Token test_token(5);
  test_token.move_to_left();
  cout << "on doit avoir 4 jetons avant l'ajout" << endl;
  for(int i = 0 ; i < test_token.get_length();i ++)
  {
    cout << test_token.get_bufferToken()[i];
  }
  cout << endl;
  test_token.add_Token();
  cout << "on doit avoir 5 jetons après l'ajout" << endl;
  for(int i = 0 ; i < test_token.get_length();i ++)
  {
    cout << test_token.get_bufferToken()[i];
  }
  cout << endl;
}

/** Entrées : aucune
* sortie aucune
* crée un objet Token ayant pour taille 5
* exécute la commande to_String qui doit afficher
* 5 "1"
* auteur : Dylan
**/
void test_to_String()
{
  Token test_token(5);
  cout << "on doit avoir 5 '1'" << endl;
  cout << test_token.to_String() << endl;
}

// Test du constructeur vide.
void test_void_constructor_cell()
{
  Cell test_cell;
  cout << "Constructeur vide fait !" << endl;
}

// Test du constructeur avec les paramètres priority et type.
void test_constructor(int priority, int type)
{
  Cell test_cell(priority,type);
  cout << "Constructor with parameters done!" << endl;
}

// Test de la fonction getPriority.
void test_get_priority()
{
  Cell test_cell(0,3);
  cout << "On doit obtenir 0. On a test_cell.priority = " << test_cell.getPriority() << endl;
}

// Test de la fonction getType.
void test_get_type()
{
  Cell test_cell(0,3);
  cout << "On doit obtenir 3. On a test_cell.type = " << test_cell.getType() << endl;
}

// Test de la fonction setPriority. On créé une cellule avec le constructeur ave paramètres puis on modifie la priorité.
void test_set_priority(int priority)
{
  Cell test_cell(0,3);
  cout << "On doit obtenir 0 avant le set : test_cell.priority = " << test_cell.getPriority() << endl;
  test_cell.setPriority(priority);
  cout << "On doit obtenir la nouvelle priorite apres le set : test_cell.priority = " << test_cell.getPriority() << endl;
}

// Test de la fonction setType. On créé une cellule avec le constructeur ave paramètres puis on modifie le type.
void test_set_type(int type)
{
  Cell test_cell(0,3);
  cout << "On doit obtenir 3 avant le set : test_cell.type = " << test_cell.getType() << endl;
  test_cell.setType(type);
  cout << "On doit obtenir le nouveau type apres le set : test_cell.type = " << test_cell.getType() << endl;
}

void test_void_constructor_BufferCell()
{
  BufferCell testBufferCell;
  cout << "constructeur vide fait" << endl;
}

void test_constructor_BufferCell(int aLength)
{
  BufferCell testBufferCell(aLength);
  cout << "constructeur avec une taille fait" << endl;
}

void test_to_string_BufferCell()
{
  BufferCell testBufferCell(5);
  cout << testBufferCell.to_String() << endl;
}

void test_add_Cell(Cell aCell)
{
  BufferCell testBufferCell(5);
  testBufferCell.add_Cell(aCell);
  cout << testBufferCell.to_String() << endl;
  testBufferCell.add_Cell(aCell);
  testBufferCell.add_Cell(aCell);
  testBufferCell.add_Cell(aCell);
  testBufferCell.add_Cell(aCell);
  cout << testBufferCell.to_String() << endl;
  testBufferCell.add_Cell(Cell(1,2));
  cout << testBufferCell.to_String() << endl;
}

void test_take_Cell()
{
  BufferCell testBufferCell(5);
  testBufferCell.add_Cell(Cell(0,2));
  testBufferCell.add_Cell(Cell(0,2));
  testBufferCell.add_Cell(Cell(0,2));
  testBufferCell.add_Cell(Cell(0,2));
  testBufferCell.add_Cell(Cell(0,2));
  cout << testBufferCell.to_String() << endl;
  testBufferCell.take_Cell();
  cout << testBufferCell.to_String() << endl;
}

void test_move_to_left_bufferCell()
{
  BufferCell testBufferCell(5);
  testBufferCell.add_Cell(Cell(0,2));
  testBufferCell.add_Cell(Cell(0,2));
  testBufferCell.add_Cell(Cell(0,2));
  testBufferCell.add_Cell(Cell(0,2));
  testBufferCell.add_Cell(Cell(0,2));
  cout << testBufferCell.to_String() << endl;
  testBufferCell.move_to_left();
  cout << testBufferCell.to_String() << endl;
}

void test_constructor_void_client()
{
  Client test_client;
  cout << "constructeur vide de client fait !" <<endl;
}

void test_constructor_client (int id, float transmitRate, float geneTokensRate)
{
  Client test_client(id,transmitRate,geneTokensRate);
  cout << "constructeur de client fait !" <<endl;
}

void test_get_on()
{
  Client test_client(1,2,3);
  cout << "Le client doit être false" << test_client.get_on() <<endl;
}

void test_set_on(bool aBool)
{
  Client test_client(1,2,3);
  cout << "Le client doit être true" << test_client.get_on() <<endl;
  test_client.set_on(aBool);
}

void test_get_idClient()
{
  Client test_client(1,2,3);
  cout << "l'id client doit être 1 " << test_client.get_idClient() << endl;
}

void test_set_idClient(int idClient)
{
  Client test_client(1,2,3);
  test_client.set_idClient(idClient);
  cout << "l'id client doit être 2 " << test_client.get_idClient() << endl;
}

void test_get_tokens_rate()
{
  Client test_client(1,2,3);
  cout << "le taux de génération des jetons doit être 3 " << test_client.get_tokens_rate() <<endl;
}

void test_set_tokens_rate(float tokensRate)
{
  Client test_client(1,2,3);
  test_client.set_tokens_rate(tokensRate);
  cout << "le taux de génération des jetons doit être 50.2 " << test_client.get_tokens_rate() <<endl;
}

void test_get_transmit_rate()
{
  Client test_client(1,2,3);
  cout << "le taux de génération des jetons doit être 2 " << test_client.get_transmit_rate() <<endl;
}

void test_set_transmit_rate(float transmitRate)
{
  Client test_client(1,2,3);
  test_client.set_transmit_rate(transmitRate);
  cout << "le taux de génération des jetons doit être 42 " << test_client.get_transmit_rate() <<endl;
}

void test_get_nb_cell_transmit()
{
  Client test_client(1,2,3);
  cout << "le nombre de cellules transmisent doit être 0 " << test_client.get_nb_cell_transmit() <<endl;
}

void test_set_nb_cell_transmit (int nbCellTransmit)
{
  Client test_client(1,2,3);
  test_client.set_nb_cell_transmit(nbCellTransmit);
  cout << "le nombre de cellules transmisent doit être 5000 " << test_client.get_nb_cell_transmit() <<endl;
}

void test_get_nb_lost_cell()
{
  Client test_client(1,2,3);
  cout << "le nombre de cellules perdues doit être 0 " << test_client.get_nb_lost_cell() <<endl;
}

void test_set_nb_lost_cell(int nbCellLost)
{
  Client test_client(1,2,3);
  test_client.set_nb_lost_cell(nbCellLost);
  cout << "le nombre de cellules perdues doit être 80 " << test_client.get_nb_lost_cell() <<endl;
}
