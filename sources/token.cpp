#include "../headers/token.h"

/** Entrées : aucune
* sortie aucune
* constructeur vide de token
**/
Token::Token(){}

/** Entrées : int length
* sortie aucune
* constructeur de token avec un paramètre entier
* assigne la taille du tableau de jetons et l'initialise rempli de jeton
**/
Token::Token(int length)
{
  this->length = length;
  this->bufferToken = new bool[length];
  for(int i=0 ; i < this->length ; i++)
  {
    bufferToken[i] = true;
  }
}

/** Entrées : aucune
* sortie aucune
* destructeur token, détruit les QGraphicsEllipseItem
**/
Token::~Token()
{
  //delete(this->tokenGraph);
}

/** Entrées : aucune
* sortie int length
* accesseur de length, retourne length
**/
int Token::get_length()
{
  return this->length;
}

/** Entrées : aucune
* sortie QGraphicsEllipseItem*
* accesseur de tokenGraph, retourne tokenGraph contenant les
* QGraphicsEllipseItem positionnant les jetons dans la fenêtre
**/
/*QGraphicsEllipseItem* Token::get_tokenGraph()
{
  return this->tokenGraph;
}*/

/** Entrées : aucune
* sortie bool* bufferToken
* accesseur de bufferToken, retourne bufferToken
**/
bool* Token::get_bufferToken()
{
  return this->bufferToken;
}

/** Entrées : aucune
* sortie aucune
* ajoute un jeton dans le tableau en mettant le 1er bool faux à vrai
* parcous de 0 à la taille du tableau
**/
void Token::add_Token()
{
  for(int i=0 ; i < this->length ; i++)
  {
    if(this->bufferToken[i] == false)
    {
      bufferToken[i] = true;
      break;
    }
  }
}

/** Entrées : aucune
* sortie aucune
* décale les jetons vers la droite
* la valeur de la case courante prend la valeur de la case d'après
**/
void Token::move_to_left()
{
  for(int i=0 ; i < this->length-1 ; i++)
  {
    this->bufferToken[i] = this->bufferToken[i+1];
  }
  this->bufferToken[this->length-1] = false;
}

/** Entrées : aucune
* sortie String tokens
* retourne sous forme de texte le tableau de jetons
* 1 il y a un jeton, 0 il n'y en a pas
**/
string Token::to_String()
{
  string tokens("");

  for(int i=0; i< this->length; i++)
  {
    if(this->bufferToken[i]==true)
    {
      tokens += "1 ";
    }
    else
    {
      tokens += "0 ";
    }
  }
  return tokens;
}
