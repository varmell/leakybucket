#include "../headers/bufferCell.h"

/**
* Constructeur vide de BufferCell
* Entrées : aucune
* Sortie : aucune
*/
BufferCell::BufferCell(){}

/**
* Constructeur de BufferCell
* construit un objet BufferCell de taille aLength
* rempli celui-ci de cellule de priorité -1 et de type 0 qui correspond à une cellule vide
* Entrées : entier pour la taille du buffer de cellules
* Sortie : aucune
*/
BufferCell::BufferCell(int aLength)
{
  this->length = aLength;
  this->cells = new Cell[this->length];
  for(int i = 0; i < this->length ; i++)
  {
    this->cells[i] = Cell(-1,0);
  }
}

/**
* destructeur de BufferCell
* Entrées : aucune
* Sortie : aucune
*/
BufferCell::~BufferCell(){}

/**
* Ajoute une cellule à l'objet BufferCell
* Parcours une première fois l'ensemble des cellules à la recherche d'une place vide
* puis une seconde fois de la 1er cellule à la dernière en remplacer la 1er cellule
* rencontrer de plus basse priorité par la cellule à ajouter.
* Entrées : une cellule
* Sortie : un booléen, vrai si une cellule a du être enlevé pour l'ajout
* faux s'il y avait une place libre
*/
bool BufferCell::add_Cell(Cell aCell)
{
  int i = 0;
  while(i < this->length)
  {
    if(this->cells[i].getType() == 0)
    {
      this->cells[i] = aCell;
      return false;
    }
    i++;
  }
  i--;
  while(i > 0)
  {
    if(this->cells[i].getPriority() >= 0)
    {
      this->cells[i] = aCell;
      return true;
    }
    i--;
  }
  return false;
}

/**
* Supprime la 1er cellule de l'objet BufferCell
* remplace la 1er cellule par la cellule vide
* Entrées : aucune
* Sortie : aucune
*/
void BufferCell::take_Cell()
{
  this->cells[0] = Cell(-1,0);
}

/**
* déplace les cellules d'une case vers la gauche
* et remplace la cellules de fin par la cellule vide.
* La cellule de tête est ainsi perdue
* Entrées : aucune
* Sortie : aucune
*/
void BufferCell::move_to_left()
{
  for(int i=0 ; i < this->length-1 ; i++)
  {
    this->cells[i] = this->cells[i+1];
  }
  this->cells[this->length-1] = Cell(-1,0);
}

/**
* Renvoie sous forme de chaîne de caractères des infos sur le buffer de cellules
* la taille et chaque cellule sous la forme (priorité,type)
* Entrées : aucune
* Sortie : string
*/
string BufferCell::to_String()
{
  string buffer("Il peut y avoir au maximum " + std::to_string(this->length));

  for(int i=0; i < this->length;i++)
  {
    buffer +=" ("+std::to_string(this->cells[i].getPriority())+","+std::to_string(this->cells[i].getType())+")";
  }
  return buffer;
}

/**
* indique si le buffer de cellules est vide
* renvoie vrai si la celluyle de tête est une cellule vide
* sinon faux
* Entrées : aucune
* Sortie : un booléen
*/
bool BufferCell::cells_void()
{
  return this->cells[0].getPriority() == -1;
}
